package com.example.springlogging.controller;

import com.example.springlogging.dtos.CreateUserRequestDTO;
import com.example.springlogging.dtos.CreateUserResponseDTO;
import com.example.springlogging.services.IUserServices;
import com.example.springlogging.services.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class UserController {
    @Autowired
    UserServices userServices;

    @GetMapping("/users")
    public ResponseEntity<List<CreateUserResponseDTO>> getAllUsers() {
        return new ResponseEntity<>(userServices.getAllUsers(), HttpStatus.OK);
    }

    @GetMapping("/users/{userId}")
    public ResponseEntity<CreateUserResponseDTO> getUserById(@PathVariable("userId") Long userId) {
        return new ResponseEntity<>(userServices.getUserById(userId), HttpStatus.OK);
    }

    @PostMapping("/users")
    public ResponseEntity<CreateUserResponseDTO> createUser(@RequestBody CreateUserRequestDTO requestDTO) {
        return new ResponseEntity<>(userServices.createNewUser(requestDTO), HttpStatus.CREATED);
    }

    @PutMapping("/users/{userId}")
    public ResponseEntity<CreateUserResponseDTO> updateUser(@RequestBody CreateUserRequestDTO requestDTO, @PathVariable("userId") Long userId) {
        return new ResponseEntity<>(userServices.updateUserById(requestDTO, userId), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("users/{userId}")
    public ResponseEntity<String> deletedUser(@PathVariable("userId") Long userId) {
        userServices.deleteUserById(userId);
        return new ResponseEntity<>("User deleted successfully!", HttpStatus.ACCEPTED);
    }

}
