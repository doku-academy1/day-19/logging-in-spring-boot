package com.example.springlogging.dtos;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class CreateUserRequestDTO implements Serializable {
    private String username;
    private String email;
    private String password;
}
